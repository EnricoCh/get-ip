# PUB-IP
![Apache 2.0 License](https://img.shields.io/static/v1.svg?label=📃%20License&message=Apache-2.0&color=informational)  


Pub-ip is a [command-line utility](https://www.rust-lang.org/what/cli) to get the public 
[ip address](https://en.wikipedia.org/wiki/IP_address) 
of your machine when your machine 
is hidden behind a [NAT](https://en.wikipedia.org/wiki/Network_address_translation), 
like home computers usually are.


> **Warning**
> This tool uses third-party services to retrieve the public IP address


## Installation
You can install this program using cargo:
```bash
cargo install pub-ip
```
In the future, I'd like to provide pre-built executables on this progect repository using CI/CD


## Usage
Simply run `pub-ip` on your terminal.

You can specify which server to use:
 - `pub-ip ipify`
 - `pub-ip httpbin`
 - `pub-ip myip`
 - `pub-ip ifconfig`
  
When no server is specified, all servers are queried, in the above order, until there is a result.

By default, when all servers fail, pub-ip outputs `NOTFOUND`.\
You can override this behavior by using the `--notfound` flag (or `-f`):

 - `pub-ip ipify --notfound` or
 - `pub-ip ipify -f`

By default pub-ip prints a newline character at the end.\
You can override this behaviour by using the `--newline` flag (or `-l`)

 - `pub-ip ipify --newline` or
 - `pub-ip ipify -l`

Use `pub-ip --help` to display help informations.

Use `pub-ip --version` to display version informations.


## License
Licensed under 
Apache License, Version 2.0 (LICENSE-APACHE or http://apache.org/licenses/LICENSE-2.0)

## Contribution
Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be
licensed as above, without any additional terms or conditions.
