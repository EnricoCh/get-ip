use clap::{ArgEnum, Parser};
use std::collections::HashMap;

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
struct Args {
    /// Which server to use. Omit it to try all of them.
    #[clap(arg_enum)]
    server: Option<Server>,

    /// Do not print a newline character at the end
    #[clap(short = 'l', long)]
    newline: bool,

    /// Do not print `NOTFOUND`
    #[clap(short = 'f', long)]
    notfound: bool,
}

#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, ArgEnum, Debug)]
enum Server {
    Ipify,
    Httpbin,
    Myip,
    Ifconfig,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let cli = Args::parse();

    let found: bool = match cli.server {
        Some(value) => match value {
            Server::Ipify => {
                if let Some(ip) = make_request_ipfy() {
                    print!("{ip}", ip = ip);
                    true
                } else {
                    false
                }
            }
            Server::Httpbin => {
                if let Some(ip) = make_request_httpbin() {
                    print!("{ip}", ip = ip);
                    true
                } else {
                    false
                }
            }
            Server::Myip => {
                if let Some(ip) = make_request_myip() {
                    print!("{ip}", ip = ip);
                    true
                } else {
                    false
                }
            }
            Server::Ifconfig => {
                if let Some(ip) = make_request_ifconfig() {
                    print!("{ip}", ip = ip);
                    true
                } else {
                    false
                }
            }
        },
        None => {
            if let Some(ip) = make_request_ipfy() {
                print!("{ip}", ip = ip);
                true
            } else if let Some(ip) = make_request_httpbin() {
                print!("{ip}", ip = ip);
                true
            } else if let Some(ip) = make_request_myip() {
                print!("{ip}", ip = ip);
                true
            } else {
                false
            }
        }
    };

    if !cli.notfound && !found {
        print!("NOTFOUND");
    }
    if !cli.newline {
        println!("");
    }
    Ok(())
}

/// makes a request to "https://api.ipify.org?format=json"
fn make_request_ipfy() -> Option<String> {
    let resp = reqwest::blocking::get("https://api.ipify.org?format=json").ok();
    if let Some(resp) = resp {
        let resp = resp.json::<HashMap<String, String>>();
        if let Ok(resp) = resp {
            if let Some(ip) = resp.get("ip") {
                return Some(ip.to_string());
            }
        }
        return None;
    }
    None
}
/// makes a request to "https://httpbin.org/ip"
fn make_request_httpbin() -> Option<String> {
    let resp = reqwest::blocking::get("https://httpbin.org/ip").ok();
    if let Some(resp) = resp {
        let resp = resp.json::<HashMap<String, String>>();
        if let Ok(resp) = resp {
            if let Some(ip) = resp.get("origin") {
                return Some(ip.to_string());
            }
        }
        return None;
    }
    None
}
/// makes a request to "https://api.my-ip.io/ip"
fn make_request_myip() -> Option<String> {
    let resp = reqwest::blocking::get("https://api.my-ip.io/ip").ok();
    if let Some(resp) = resp {
        let resp = resp.text();
        if let Ok(resp) = resp {
            return Some(resp);
        }
    }
    None
}
/// makes a request to "https://ifconfig.me/ip"
fn make_request_ifconfig() -> Option<String> {
    let resp = reqwest::blocking::get("https://ifconfig.me/ip").ok();
    if let Some(resp) = resp {
        let resp = resp.text();
        if let Ok(resp) = resp {
            return Some(resp);
        }
    }
    None
}

#[test]
fn verify_app() {
    use clap::CommandFactory;
    Args::command().debug_assert()
}
